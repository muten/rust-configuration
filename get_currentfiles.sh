#! /bin/sh

mycp_etc()
{
    directory=`dirname $1`

    if [ ! -d $directory ]
    then mkdir -pv $directory
    fi

    cp -v /$1 $1
}

mycp_steamdir_rust()
{
    directory=steamdir/`dirname $1`

    if [ ! -d $directory ]
    then mkdir -pv $directory
    fi

    cp -v /home/admin_cs/$1 steamdir/$1
}

mycp_etc etc/rust-server-launcher/rust-server-launcher.conf
mycp_etc etc/init.d/rust-server-launcher

mycp_steamdir_rust rust/cfg/server.cfg

